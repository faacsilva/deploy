import os
import pyodbc
import sys

# Checa a presenca do ip da maquina via command line arqument
if(len(sys.argv) == 1):
	print("Para executar o deploy, voce precisa indicar qual e a maquina!")
	sys.exit();

# Forca salvar as credenciais do GIT
os.system('git config credential.helper store')

# Conecta no Banco
cnpg = pyodbc.connect('DSN=MISPG')

# Consultas
strsql_1 = "SELECT savepath FROM deploy.nodes WHERE status=1 AND node='"+str(sys.argv[1])+"'"
strsql_2 = "SELECT source FROM deploy.projects WHERE status=1"

# Percorre a tabela de nodes para obter o save path deste
for node in cnpg.execute(strsql_1):
	# Entra no savepath do node
	os.chdir(node.savepath)

	# Percorre a tabela de aplicacoes para obter o codigo fonte
	for project in cnpg.execute(strsql_2):
		# executa o deploy propriamente dito
		os.system('git clone ' + project.source) 

cnpg.commit()